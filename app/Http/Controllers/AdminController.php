<?php

namespace App\Http\Controllers;
use Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Image;
use App\Skill;
use App\Project;
use App\Category;
use App\ProjectSkill;
use Illuminate\Support\Facades\DB;
class AdminController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function admin(){
		$projects = Project::all();
		$categories = Category::all();
		$skills = Skill::all();
		$images = Image::all();
		$projectSkills = ProjectSkill::all();

		return view('admin',['categories'=>$categories,
		'skills'=>$skills,
		'projects'=>$projects,
		'images'=>$images,
		'projectSkills'=>$projectSkills,

		]);
	}

	public function setProject(Request $request){
		$validator = Validator::make($request->all(),[
	        'name'          => 'required|string',
	        'URL'          => 'url',
			'date'     => 'date',
			'description' => 'required',
			'categories' =>'required',
			'skills' =>'required',
		    'images.*' => 'image|max:4000',
		]);
		if ($validator->fails()) {
            return redirect('admin')
                        ->withErrors($validator)
                        ->withInput();
        }
		$project = new Project(['name'=>$request->name,'client'=>$request->client,'URL'=>$request->URL,'description'=>$request->description,'date'=>$request->date,
								'category_id'=>$request->categories]);
		$project->save();
		if(count($request->input('skills'))>0){
			foreach($request->input('skills') as $skill ){
				$projectSkill = new ProjectSkill(['project_id'=>$project->id,'skills_id'=>$skill]);
				$projectSkill->save();
			}
		}
		if(count($request->file('images'))>0){
			foreach($request->file('images') as $image){
				$path = $image->store('public');
				$url = Storage::url($path);
				$newImage = new Image(['name'=>$url,'project_id'=>$project->id]);
				$newImage->save();
			}
		}
		return redirect('/admin');	
	}


	public function editProject($id){
		$project = Project::find($id);
		$categories = Category::all();
		$skills = Skill::all();
		$projectSkills = ProjectSkill::where('project_id','=',$id)->select('skills_id')->get();
		return view('edit_project',['categories'=>$categories,
		'skills'=>$skills,
		'project'=>$project,
		 'projectSkills'=>$projectSkills,
		]);

	}
	public function updateProject(Request $request,$id){
		$validator = Validator::make($request->all(),[
			'name'          => 'required|string',
			'URL'          => 'url',
			'date'     => 'date',
			'categories' =>'required',
			'description' => 'required',
			'images'=> 'nullable',
			'images.*' => 'image|max:4000',
		]);
		if ($validator->fails()) {
            return redirect('admin')
                        ->withErrors($validator)
                        ->withInput();
        }
		Project::where('id',$id)->update(['name'=>$request->name,'client'=>$request->client,'URL'=>$request->URL,
		'description'=>$request->description,
		'date'=>$request->date,'category_id'=>$request->categories]);

		ProjectSkill::where('project_id',$id)->delete();
		if(count($request->input('skills'))>0){
			foreach($request->input('skills') as $skill ){
				$projectSkill = new ProjectSkill(['project_id'=>$id,'skills_id'=>$skill]);
				$projectSkill->save();
			}
		}
		if(count($request->file('images'))>0){
			foreach($request->file('images') as $image){
				$path = $image->store('public');
				$url = Storage::url($path);
				$newImage = new Image(['name'=>$url,'project_id'=>$id]);
				$newImage->save();
				}
		}
		return redirect('/admin/');
	}


	public function setCategory(Request $request){
		$validator = Validator::make($request->all(),[
			'name'=>'required|string|unique:categories,name',
		]);

		if ($validator->fails()) {
			return redirect('admin')
						->withErrors($validator,'add_cat')
						->withInput();
		}
		$category = new Category(['name'=>$request->name]);
		$category->save();
		return redirect('/admin');
	}

	public function updateCategory(Request $request,$id){
		$validator = Validator::make($request->all(),[
			'name'=>'required|string|unique:categories,name',
		]);
		if ($validator->fails()) {
            return redirect('admin')
                        ->withErrors($validator)
                        ->withInput();
        }
		$category = Category::find($id);
		$category->update(['name'=>$request->name]);
		$category->save();
		return redirect('/admin');

	}

	public function setSkill(Request $request){
		$validator = Validator::make($request->all(),[
			'name'=>'required|string|unique:skills,name',
		]);
		if ($validator->fails()) {
            return redirect('admin')
                        ->withErrors($validator,'add_skill')
                        ->withInput();
        }
		$skill = new Skill(['name'=>$request->name]);
		$skill->save();
		return redirect('/admin');
	}

	public function updateSkill(Request $request,$id){
		$validator = Validator::make($request->all(),[
			'name'=>'required|string|unique:skills,name',
		]);
		if ($validator->fails()) {
            return redirect('admin')
                        ->withErrors($validator)
                        ->withInput();
        }

		$skill = Skill::find($id);
		$skill->update(['name'=>$request->name]);
		$skill->save();
		return redirect('/admin');
	}
	public function deleteProject($id){
		$project = Project::find($id);
		$images = $project->images;
		foreach($images as $image){
			$image_name = explode('/',$image->name)[2];
			Storage::delete($image_name);
			if(!Storage::exists($image_name)){
				if(Image::destroy($image->id)){
					continue;
				}
				else {
					return response()->json(['success'=>false]);
				}
			}
			else{
				return response()->json(['success'=>false]);
			}
		}
		if(Project::destroy($id))
			return response()->json(['success'=>true]);
		else{
			return response()->json(['success'=>false]);
		}
	}

	public function deleteCategory($id){
		$category = Category::findorfail($id);
		if(count($category->projects)){
			return response()->json(['success'=>false,'error'=>'This category is still in use by another project.']);
		}
		if(Category::destroy($id))
			return response()->json(['success'=>true]);
		else{
			return response()->json(['success'=>false]);
		}
	}

	public function deleteSkill($id){
		if(Skill::destroy($id))
			return response()->json(['success'=>true]);
		else{
			return response()->json(['success'=>false]);
		}
	}

	public function deleteImage($id){
		$image = Image::find($id);
		$image_name = explode('/',$image->name)[2];
		Storage::delete($image_name);
		if(!Storage::exists($image_name)){
			if(Image::destroy($id))
			return response()->json(['success'=>true]);
		}
		else{
			return response()->json(['success'=>false]);
		}
	}
}
