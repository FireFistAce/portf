<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;
use App\Skill;
use App\Project;
use App\Category;
use App\ProjectSkill;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$count = Project::count();
		$skip=3;
		$limit = $count-$skip;
		if($limit < 0){
			$limit = 0;
		}
		$categories = Category::all();
		$projects = Project::skip($skip)->take($limit)->get();
		$topProjects = Project::all()->take($skip);
        return view('home',['projects'=>$projects,'topProjects'=>$topProjects,'categories'=>$categories]);
    }
}
