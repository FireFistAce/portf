<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Image;
class ProjectController extends Controller
{
    public function getProject($id){
		$project = Project::findorfail($id);
		$next = Project::find($id+1);
		$prev = Project::find($id-1);
		return view('project',['project'=>$project,'next'=>$next,'prev'=>$prev]);
	}
	public function getAllProjects(){

	}
	public function getProjectImages($id){
		$projectImages = Image::where('project_id','=',$id)->get();
		return $projectImages;
	}

}
