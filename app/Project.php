<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Image;
use App\ProjectCategory;
use App\Category;
class Project extends Model
{
    protected $guarded = [];
	protected $appends = array('firstimage','images');
	public function GetFirstimageAttribute(){
		return Image::where('project_id','=',$this->attributes['id'])->first();
	}
	public function GetImagesAttribute(){
		return Image::where('project_id','=',$this->attributes['id'])->get();
	}
	public function category(){
		return $this->belongsTo('App\Category');
	}
	public function skills(){
		return $this->belongsToMany('App\Skill','project_skills','project_id','skills_id');
	}
}
