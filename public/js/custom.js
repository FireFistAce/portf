/* var btn = $.fn.button.noConflict() // reverts $.fn.button to jqueryui btn
$.fn.btn = btn // assigns bootstrap button functionality to $.fn.btn */
$(document).ready(function(){

    $('.delete-project-button').on('click',function(){
		const button =$(this);
		const id = $(this).parents('.projects-table tr').find('#id').html();
		const row = button.parents ('.projects-table tr') ;
		const newmodal = jQuery ('.delete-confirm-modal');
		newmodal.modal('show');
		newmodal.find('.yes-button').bind('click',{currentId:id,modal:newmodal,currentRow:row},function(event){
			$.ajax({
		            url:'../project/'+event.data.currentId+'/delete',
		            type:'get',
		            dataType:'json',
		            success : function(response){
				                if(response.success){
									event.data.currentRow.detach();
									event.data.modal.modal('hide');
									location.reload();
								}
								else{
									alert('Something went wrong please try again later')
									location.reload();
								}

		            		},
					error : function(){
							alert("Server error. Please try again later.");
							location.reload();
					},
		    });
		});

	});
	$('.project-img-thumbnail').on('click',function(){
		console.log("image");
		const imageModal = $('.show-image-modal');
		const img = imageModal.find('img');
		const currentImg = $(this);
		const id = $(this).attr('data-image-id');
		img.attr('src',$(this).attr('src'));
		imageModal.modal('show');
		imageModal.find('.delete-button').one('click',function(){
			if(confirm("Are you sure you want to delete this image?"))
			{
				$.ajax({
					url:'/image/'+id+'/delete',
					type:'get',
					dataType:'json',
					success : function(response){
						if(response.success){
							console.log(imageModal);
							imageModal.modal('hide');
						    location.reload();
						}
						else{
							imageModal.modal('hide');
							alert("Something went wrong. Please try again later");
							location.reload();
						}

					},
					error : function(){
							alert("Server error. Please try again later.");
							location.reload();
					}
				});
			}
			else{
				newModal.modal('hide');
			}

		});
	});
	$('.edit-category').on('click',function(){
		const editModal = $('.edit-category-modal');
		const text = $(this).html();
		const id = $(this).attr('data-id');
		editModal.modal('show');
		console.log('here');
		const inp = editModal.find('#name');
		inp.val(text);
		editModal.find('.edit-category-form').attr('action','category/'+id+'/update');
		editModal.find('.delete-button').one('click',function(){
			if(confirm("Are you sure you want to delete this category?"))
			{
				$.ajax({
					url:'/category/'+id+'/delete',
					type:'get',
					dataType:'json',
					success : function(response){
						if(response.success){
							editModal.modal('hide');
							location.reload();
						}
						else{
							editModal.modal('hide');
							alert("Something went wrong. Please try again later");
							location.reload();
						}

					},
					error : function(){
							alert("Server error. Please try again later.");
							location.reload();
					}
				});
			}
			else{
				editModal.modal('hide');
			}

		});
	});
	$('.edit-skill').on('click',function(){
		console.log('here');
		const editModal = $('.edit-skill-modal');
		const text = $(this).html();
		const id = $(this).attr('data-id');
		editModal.modal('show');
		console.log('here');
		const inp = editModal.find('#name');
		inp.val(text);
		editModal.find('.edit-category-form').attr('action','skill/'+id+'/update');
		editModal.find('.delete-button').one('click',function(){
			if(confirm("Are you sure you want to delete this Skill?"))
			{
				$.ajax({
					url:'/skill/'+id+'/delete',
					type:'get',
					dataType:'json',
					success : function(response){
						if(response.success){
							editModal.modal('hide');
							location.reload();
						}
						else{
							editModal.modal('hide');
							alert("Something went wrong. Please try again later");
							location.reload();
						}

					},
					error : function(){
							alert("Server error. Please try again later.");
							location.reload();
					},
				});
			}
			else{
				editModal.modal('hide');
			}

		});


	});


});
