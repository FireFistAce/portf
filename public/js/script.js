$(window).on('load',function() {
});
$(document).ready(function() {
	var hammerswipe = new Hammer($('.carousel-inner').get(0));
	hammerswipe.on('swipe', function(ev) {
		console.log(ev.direction);
		if(ev.direction == 2){
			$('#myCarousel').carousel('next');
		}
		if(ev.direction == 4){
			$('#myCarousel').carousel('prev');
		}
	});
	$('.main-content').fullpage({
		fixedElements:'.navbar-primary',
		navigation:true,
		navigationPosition:'right',
		afterRender: function(){
			$('.h-slider').each(function(){
				$(this).slick({
					dots: true,
					speed: 300,
					infinite:false,
					slidesToShow: 3,
					autoplay:true,
					slidesToScroll: 1,
					responsive: [
					{
					  breakpoint: 1024,
					  settings: {
						slidesToShow: 3,
						slidesToScroll: 1,
						infinite: false,
						dots: true
					  }
					},
					{
					  breakpoint: 600,
					  settings: {
						slidesToShow: 2,
						slidesToScroll: 1
					  }
					},
					{
					  breakpoint: 480,
					  settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					  }
					}
					// You can unslick at a given breakpoint now by adding:
					// settings: "unslick"
					// instead of a settings object
					]
				});//slick ends
			});//each() ends
		}, //afterRender() ends
	});//fullpage ends
	$(".se-pre-con").delay('1000').fadeOut("slow");

});//document ready() ends
