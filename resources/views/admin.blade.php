@extends('layouts.app')
@section('modals')
	@include('modals.delete_confirm')
	@include('modals.show_image')
	@include('modals.edit_category')
	@include('modals.edit_skill')
@endsection
@section('content')

<div class="row">
	@if(count($errors)>0)
	@foreach($errors->all() as $error)
		<li class="help-block col-lg-12" style="background-color:#fff">
			<strong class="col-lg-12" style="text-align:center">{{ $error}}</strong>
		</li>
	@endforeach
	@endif
	<div class="add-project-container col-md-offset-1 col-md-6 col-lg-offset-1 col-lg-6 col-sm-12 col-xs-12">
			<div class="panel panel-default">
		    	<div class="panel-heading">Add Project</div>
		    	<div class="panel-body">
			         @include('forms.add_project')

	    		</div><!-- panel body-->
			</div><!-- panel add project form-->
	</div>

	<div class="sidebar col-lg-4 col-md-4 col-sm-12 col-xs-12">
		<div class="row container-fluid">
			<div class="panel panel-default add-category-panel">
				<div class="panel-heading">Categories</div>
				<div class="panel-body">
					<div class='categories row'>
						@foreach($categories as $cat)
							<div class="col-lg-3 col-sm-3 col-md-3 col-xs-3">
								<button class="btn btn-default edit-category" data-id="{{$cat->id}}">{{$cat->name}}</button>
							</div>
						@endforeach
					</div>
					<div class="row">
						@include('forms.add_category')
					</div>

				</div><!-- panel body-->
			</div><!-- panel-->
			<div class="panel panel-default  add-skill-panel">
				<div class="panel-heading">Skills</div>
				<div class="panel-body">
					<div class='skills col-lg-12 col-sm-12 col-md-12 col-xs-12'>
						@foreach($skills as $skill)
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
								<button class="btn btn-default edit-skill" data-id="{{$skill->id}}">{{$skill->name}}</button>
							</div>
						@endforeach
					</div>
					@include('forms.add_skill')
				</div><!-- panel body-->
			</div><!-- panel -->
		</div>
	</div>
</div>
<div class="row">
	<div class="add-project-container col-lg-10 col-lg-offset-1 col-sm-12 col-xs-12">
		<div class="panel panel-default panel-projects">
			<div class="panel-heading">Projects</div>
			<div class="panel-body">
				@include('tables.projects')
			</div><!-- panel body-->
		</div><!-- panel add project form-->
	</div>
</div>
@endsection
