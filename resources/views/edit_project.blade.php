@extends('layouts.app')
@section('content')<div class="add-project-container col-lg-offset-2 col-md-8 col-lg-8 col-sm-offset-0 col-xs-offset-0 col-sm-12 col-xs-12">
	<div class="panel panel-default">
		<div class="panel-heading">Add Project</div>
		<div class="panel-body">
			 @include('forms.edit_project')

	</div><!-- panel body-->
	</div><!-- panel add project form-->
</div>
@endsection
