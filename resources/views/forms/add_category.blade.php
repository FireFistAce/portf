

<form id="add_category_form" class="form-horizontal" role="form" method="POST" action="{{ url('category/add') }}">
    {{ csrf_field() }}

    <div class="form-group">
        <label for="name" class="col-lg-4 col-md-4 col-sm-4 control-label">Category Name</label>

        <div class="col-lg-8">
            <input id="name" type="text" class="form-control" name="name" required autofocus>

            @if ($errors->add_cat->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->add_cat->first('name') }}</strong>
                </span>
            @endif
        </div>

    </div>
    <div class="form-group">
        <div class="col-lg-4 col-lg-offset-4">
            <button id="submit_button" type="submit" class="btn btn-sm btn-primary">
                Add
            </button>

        </div>
    </div>
</form>
