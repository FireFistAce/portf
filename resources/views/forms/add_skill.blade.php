<form id="add_skill_form" class="form-horizontal " role="form" method="POST" action="{{ url('skill/add') }}">
    {{ csrf_field() }}

    <div class="form-group">
        <label for="name" class="col-lg-4 col-md-4 col-sm-4 control-label">Skill Name</label>

        <div class="col-lg-8">
            <input id="name" type="text" class="form-control" name="name" required autofocus>

            @if ($errors->add_skill->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->add_skill->first('name') }}</strong>
                </span>
            @endif
        </div>


    </div>
    <div class="form-group">
        <div class="col-lg-4 col-lg-offset-4">
            <button id="submit_button" type="submit" class="btn btn-sm btn-primary">
                Add
            </button>

        </div>
    </div>
</form>
