<form id="edit_project_form" class="form-horizontal" role="form" method="POST" action="/project/{{$project->id}}/update	" enctype="multipart/form-data">
    {{ csrf_field() }}
	@if(count($errors)>0)
	@foreach($errors->all() as $error)
		<li class="help-block col-lg-offset-2 cl-lg-10">
			<strong>{{ $error}}</strong>
		</li>
	@endforeach
	@endif
	<input type="hidden" name="id" value="{{$project->id}}" />
    <div class="form-group">
        <label for="name" class="col-lg-2 col-md-2 col-sm-2 control-label">Project Name</label>

        <div class="col-lg-4">
            <input id="name" type="text" class="form-control" name="name" value="{{$project->name}}" required autofocus>

        </div>
        <label for="client" class="col-lg-2 control-label">Client</label>
        <div class="col-lg-4">
            <input id="client" type="text" class="form-control" name="client" value="{{$project->client}}" required>

        </div>

    </div>
	<div class="form-group">
        <label for="URL" class="col-lg-2 col-md-2 col-sm-2 control-label">Project URL(start with http://)</label>

        <div class="col-lg-4">
            <input id="URL" type="text" class="form-control" name="URL" value="{{$project->URL}}" required>

        </div>
		<label for="date" class="col-lg-2 col-md-2 col-sm-2 control-label">Project Date(yyyy-mm-dd)</label>

		<div class="col-lg-4">
			<input id="date" type="text" class="form-control" name="date" required value="{{$project->date}}">

		</div>

    </div>

	<div class="form-group">
		<label for="description" class="col-lg-2 col-md-2 col-sm-2 control-label">Project description</label>

		<div class="col-lg-10">
			<textarea id="description" type="description" class="form-control" rows="5" name="description" required>{{$project->description}}</textarea>

		</div>

	</div>
	<div class="form-group">
		<label for="categories" class="col-lg-2 col-md-2 col-sm-2 control-label">Categories</label>

		<div class="col-lg-10">

				@foreach($categories as $category)
					<input name="categories" class="project-add-checkbox" type="radio" id="category-select-{{$category->id}}"
					 value="{{$category->id}}"
						@if($project->category->id == $category->id)
							{{"checked"}}
					 	@endif
					 >
					<label for="category-select-{{$category->id}}" >{{$category->name}}</label>
				@endforeach
		</div>
	</div>

	<div class="form-group">
		<label for="skills[]" class="col-lg-2 col-md-2 col-sm-2 control-label">Skills</label>

		<div class="col-lg-10">
			@foreach($skills as $skill)
				<input name="skills[]" class="project-add-checkbox" type="checkbox" id="skill-select-{{$skill->id}}"
				value="{{$skill->id}}"
				@foreach($projectSkills as $s)
				   @if($s->skills_id == $skill->id)
					   {{"checked"}}
				   @endif
				@endforeach
				>
				<label for="skill-select-{{$skill->id}}" >{{$skill->name}}</label>
			@endforeach
		</div>
	</div>

	<div class="form-group">

		<label for="images" class="col-lg-2 col-md-2 col-sm-2 control-label">Add multiple Images</label>
		<div class="col-lg-4">
			<input type="file" name="images[]" id="images" class="" multiple>
			<label for="images" class="control-label-file">Add Images</label>
		</div>
	</div>
    <div class="form-group">
        <div class="col-lg-2 col-lg-offset-2">
            <button id="submit_button" type="submit" class="btn btn-primary">
                Update
            </button>
        </div>

		<div class="col-lg-4">
			<a href="/admin/" id="back_button" class="btn btn-default">
				Back to Admin Panel
			</a>
		</div>
    </div>
</form>
