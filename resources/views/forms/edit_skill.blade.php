
<form class="edit-skill-form form-horizontal" role="form" method="POST" action="">
    {{ csrf_field() }}

    <div class="form-group">
        <label for="name" class="col-lg-4 col-md-4 col-sm-4 control-label">Skill Name</label>

        <div class="col-lg-8">
            <input id="name" type="text" class="form-control" name="name" required autofocus>

            @if ($errors->edit_skill->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->edit_skill->first('name') }}</strong>
                </span>
            @endif
        </div>

    </div>
    <div class="form-group">
        <div class="col-lg-4 col-lg-offset-4">
            <button type="submit" class="btn btn-sm btn-primary edit_button">
                Update
            </button>

        </div>
		<div class="col-lg-4">
			<button class="delete-button btn btn-danger" type="button">
				Delete
			</button>
		</div>
    </div>
</form>
