@extends('layouts.custom')

@section('content')

	<?php $count=0;?>
	@foreach($topProjects as $tp)
		<?php $bgColor=array('#228e97','#D9B860','#1B2539'); ?>

		<div class="article-top row section" style="background-color:{{$bgColor[$count]}}" >
			<div class="col-lg-12 col-md-12 col-sm-12 com-xs-12 padding-top-container">
				<div class="article-media col-lg-6 col-md-8 col-xs-12 col-sm-12">
					<img class="img-responsive" src="@if(isset($tp->firstimage['name'])){{$tp->firstimage['name']}}@endif"/>
				</div>
				<div class="article-content col-lg-4 col-md-4 col-sm-12 col-xs-12" >
					<span class="project-name">{{$tp->name}}</span>
					<hr style="color:$fff"/>
					<p class="project-desc text-justify" style="font-size:1.2em;">{{$tp->description}}</p>
					<br/>
					<a href="{{url('/project/'.$tp->id)}}" class="btn btn-default transparent round-button">View Project</a>

				</div>
			</div>
		</div>
		<?php $count++;?>
	@endforeach


	@foreach($categories as $cat)

	@if(count($cat->projects)>0)
	<div class="projects row section " style="background-color: #333;">

		<div class="container category-heading-container">
			<span class="pretty-heading">{{$cat->name}}</span>
		</div>
		<div class="container h-slider">
				@foreach($cat->projects as $project)
					<div class="{{$project->category->name}} slick-slide container-padding" >
						<a href="{{url('/project/'.$project->id)}}" style="text-decoration:none">
						<div class="project-thumb">
							<div class="thumb-media ">
								<img class="img-responsive"
									src="@if(isset($project->firstimage['name']))
									{{$project->firstimage['name']}}
									@endif"/>
							</div>
							<div class="thumb-content ">
								<p class="text text-justify"><strong style="font-size:1.2em">{{$project->name}}:&nbsp</strong>{{$project->description}}</p>
							</div>
						</div>
						</a>
					</div>
				@endforeach
		</div>
	</div>
	@endif
	@endforeach



@endsection
