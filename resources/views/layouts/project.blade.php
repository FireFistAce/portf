<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'NVD Portfolio') }}</title>

    <!-- Styles -->
    <link href="{{url('/')}}/css/app.css" rel="stylesheet">
	<link href="{{url('/')}}/css/jquery.fullpage.min.css" rel="stylesheet" />
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
	<link href="{{url('/')}}/css/style.css" rel="stylesheet" />

	<script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body >

		@yield('modals')
		<div class="se-pre-con">

		</div>
		<nav class="navbar navbar-default navbar-fixed-top navbar-primary">
			<div class="container">
				<div class="navbar-header">
					<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
						<a class="navbar-brand" href="{{ url('/') }}">
							<img src="/images/logo.png" alt="logo">
						</a>
				</div>
				<div class="collapse navbar-collapse js-navbar-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="http://www.newvisiondigital.co/about-us/">About Us</a></li>
						<li class="dropdown dropdown-large">
							<a class="dropdown-toggle" data-toggle="dropdown">Services <span class="caret"></span></a>
							<ul class="dropdown-menu dropdown-menu-large row">
								<li class="col-sm-4 col-md-4 col-lg-4 ">
									<ul>
										<li class="dropdown-header">Digital Marketing</li>
										<li><a href="http://www.newvisiondigital.co/search-engine-optimization/">Search Engine Optimization</a></li>
										<li><a href="http://www.newvisiondigital.co/social-media-marketing/">Social Media Marketing</a></li>
										<li><a href="http://www.newvisiondigital.co/logo-design/">Pay Per CLick</a></li>
										<li><a href="http://www.newvisiondigital.co/online-reputation-management/">Online Reputation Management</a></li>
									</ul>
								</li>
								<li class="col-sm-4 col-md-4 col-lg-4">
									<ul>
										<li class="dropdown-header">Design</li>
										<li><a href="http://www.newvisiondigital.co/web-design-services/">Responsive Design</a></li>
										<li><a href="http://www.newvisiondigital.co/psd-to-html-conversion/">Logo design</a></li>
										<li><a href="http://www.newvisiondigital.co/psd-to-html-conversion/">PSD to HTML</a></li>
									</ul>
								</li>
								<li class="col-sm-4 col-md-4 col-lg-4">
									<ul>
										<li class="dropdown-header">Development</li>
										<li><a href="http://www.newvisiondigital.co/wordpress-development-services/">Wordpress Development</a></li>
										<li><a href="http://www.newvisiondigital.co/ecommerce-development-service/">Ecommerce Website</a></li>
										<li><a href="http://www.newvisiondigital.co/magento-development-services/">Magento Development</a></li>
										<li><a href="http://www.newvisiondigital.co/mobile-apps/">Mobile Apps</a></li>
									</ul>
								</li>
							</ul>

						</li>
						<li>
							<a href="http://www.newvisiondigital.co/portfolio/" >Portfolio</a>
						</li>
						<li >
							<a href="http://www.newvisiondigital.co/contact/">Contact</a>
						</li>
						<li>
							<a href="http://www.newvisiondigital.co/request-a-quote/" >Get A Free Qoute</a>
						</li>
					</ul>

				</div><!-- /.nav-collapse -->
			</div><!-- CONTAINER NAVBAR-->
		</nav>
		<div class="project-main container-fluid">

				@yield('content')


							<div class="footer row">
								<div class="call_action">
									<div class="call_action1">
										<div class="container">
											<div class="row">
												<div class="col-md-4 col-sm-4 col-xs-12">
													<div class="email05">
														<div class="email04"><span>Talk to Us!</span><br />Find out how we can help you grow your business online. Contact us by phone, email or using the contact form.</div>
													</div>
												</div>
												<div class="col-md-4 col-sm-4 col-xs-12">
													<div class="email01">
														<div class="email02"><img src="http://www.newvisiondigital.co/wp-content/themes/nvd/images/email01.png" alt="email01" /></div>
														<div class="email03"><span>E-mail</span><br /><a href="mailto:info@newvisiondigital.co">info@newvisiondigital.co</a></div>
													</div>
												</div>
												<div class="col-md-4 col-sm-4 col-xs-12">
													<div class="email01">
														<div class="email02"><img src="http://www.newvisiondigital.co/wp-content/themes/nvd/images/phone10.png" alt="phone10" /></div>
														<div class="email03"><span>Call Us</span><br /><a href="tel:+91 981 1407200"> +91 981 1407200</a></div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>



								<section  id="graid5">
									<div id="graid5-1">
										<div class="container">
											<div class="row">
												<div class="col-md-3 col-sm-3 col-xs-12">
													<div class="box2">
														<aside id="text-5" class="widget widget_text">
															<div class="textwidget">
																<div class="heading4">About us</div>
																<p>New Vision Digital is an innovative online marketing agency offering a myriad of integrated web marketing services to business across the world </p>
															</div>
														</aside>
													</div>
													<div class="box2">
														<div class="heading4">Design</div>
														<ul>
															<li><a href="http://www.newvisiondigital.co/web-design-services/">Responsive Website Design</a></li>
															<li><a href="http://www.newvisiondigital.co/logo-design/">Logo Design</a></li>
															<li><a href="http://www.newvisiondigital.co/press-release/">Press Release</a></li>
														</ul>
													</div>
												</div>
												<div class="col-md-3 col-sm-3 col-xs-12">
													<div class="box2">
														<div class="heading4">Search</div>
														<ul>
															<li><a href="http://www.newvisiondigital.co/pay-per-click-services/">Pay Per Click</a></li>
															<li><a href="http://www.newvisiondigital.co/search-engine-optimization">Search Engine Optimization</a></li>
															<li><a href="http://www.newvisiondigital.co/online-reputation-management">Online Reputation Management</a></li>
															<li><a href="http://www.newvisiondigital.co/social-media-marketing/">Social Media Optimization</a></li>
															<li><a href="http://www.newvisiondigital.co/link-removal-service/">Link Removal</a></li>
															<li><a href="http://www.newvisiondigital.co/site-map/">Site Map</a></li>
														</ul>
													</div>

													<div class="box2">
														<div class="heading4">Development</div>
														<ul>
															<li><a href="http://www.newvisiondigital.co/wordpress-development-services/">Wordpress Development</a></li>
															<li><a href="http://www.newvisiondigital.co/magento-development-services/">Magento Development</a></li>
															<li><a href="http://www.newvisiondigital.co/ecommerce-development-service/">eCommerce Development</a></li>
															<li><a href="http://www.newvisiondigital.co/psd-to-html-conversion/">PSD To Html Conversion</a></li>
														</ul>
													</div>
												</div>
												<div class="col-md-3 col-sm-3 col-xs-12">
													<aside id="text-6" class="widget widget_text">
														<div class="textwidget">
															<div class="box3">
																<div class="heading4">follow us</div>
																<a href="https://www.facebook.com/NewVisionD/" target="_blank"><img src="http://wordpress-15994-35420-90321.cloudwaysapps.com/wp-content/uploads/2015/10/facebok.png" alt="facebook" /></a>
																<a href="https://twitter.com/NVDigital" target="_blank"><img src="http://wordpress-15994-35420-90321.cloudwaysapps.com/wp-content/uploads/2015/10/twitter.png" alt="twitter" /></a>
																<a href="https://plus.google.com/+NewvisiondigitalCompany/posts" target="_blank"><img src="http://wordpress-15994-35420-90321.cloudwaysapps.com/wp-content/uploads/2015/10/google-.png" alt="google" /></a>
																<a href="https://www.linkedin.com/company/new-vision-digital-pvt-ltd" target="_blank"><img src="http://wordpress-15994-35420-90321.cloudwaysapps.com/wp-content/uploads/2015/10/linkdin1.png" alt="linkdin1" /></a>
																<a href="https://www.instagram.com/newvisiondigitalco/" target="_blank"><img src="http://www.newvisiondigital.co/wp-content/uploads/2016/11/instagram001.png" alt="Instagram" /></a>
															</div>

															<div class="box2">
																<div class="heading4">Accrediations</div>
																<div class="footer-partner">
																	<a href="https://www.google.co.in/partners/#a_profile;idtf=1408459396" target="_blank"><img src="http://www.newvisiondigital.co/wp-content/uploads/2016/02/google1.jpg" alt="google1" /></a>
																</div>
																<div class="footer-partner"><img src="http://www.newvisiondigital.co/wp-content/uploads/2016/02/BING.jpg" alt="bing" /></div>
																<div class="footer-partner">
																	<a title="Nasscom Member" href="http://www.nasscom.in/" target="_blank"><img alt="Nasscom Member" src="http://www.newvisiondigital.co/wp-content/uploads/2016/04/IMA1.jpg" /></a>
																</div>
																<div class="footer-partner">
																	<a title="Internet Marketing Association Member" href="http://imanetwork.org/" target="_blank"><img alt="Internet Marketing Association Member" src="http://www.newvisiondigital.co/wp-content/uploads/2016/04/IMA2.jpg" /></a>
																</div>

															</div>
														</div>
													</aside>
												</div>
												<div class="col-md-3 col-sm-3 col-xs-12">
													<div class="box4">
														<aside id="text-7" class="widget widget_text">
															<div class="textwidget">
																<div class="heading4">offices</div>
																<div id='hcard_embed'>
																	<div id="hcard-India" class="vcard">
																		<div class="fn n"><span class="given-name">India</span> </div>

																		<div class="adr">
																			<div class="street-address">C-89c, IIIrd Floor, Sector-8</div>
																			<span class="locality">Noida - </span><span class="postal-code"> 201301</span>
																			<div class="tel"><span class="type">Ph:</span><a href="tel:+91 120 4115003">+91 120 4115003</a></div>
																		</div>
																	</div>
																</div>
															</div>
														</aside> <img src="http://www.newvisiondigital.co/wp-content/uploads/2015/10/footer-logo.png" width="100%" alt="logo" />
													</div>
												</div>

											</div>


										</div>
									</div>
								</section >

								<section id="graid6">
									<div id="graid6-1">
										<div class="container">
											<div class="row">
												<div class="col-md-12 col-sm-12 col-xs-12">
													<p>© 2016 NEW VISION DIGITAL PVT LTD | <a href="http://www.newvisiondigital.co/privacy-policy">Privacy Policy </a>| <a href="http://www.newvisiondigital.co/disclaimer">Disclaimer</a>
														<div id="copyright">
															<div id="DMCA-badge" style="text-shadow: none!important; font-family: Helvetica, Arial, sans-serif!important; text-transform: capitalize; font-weight: bold; font-size: 11px;">
																<div class="badgeleft" style="left: 0px; float: left; padding: 5px; background-color: rgb(53, 187, 203);">
																	<a href="http://www.dmca.com/" title="DMCA" style="color: #FFF;">DMCA</a></div>
																<div class="badgeright" style="float: left; padding: 5px; border-left-width: 1px; border-left-style: solid; border-left-color: rgb(255, 255, 255); background-color: rgb(0, 0, 0);">
																	<a href="http://www.dmca.com/ProtectionPro.aspx" title="DMCA" style="color: rgb(255, 255, 255);">PROTECTED</a></div>
													</p>
													</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</section >

					</div>
			</div>

	<script src="{{url('/')}}/js/jquery-3.1.1.min.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<script src="{{url('/')}}/js/jquery.fullpage.min.js"></script>
	<script src="{{url('/')}}/js/hammer.min.js"></script>
	<script src="{{url('/')}}/js/script.js"></script>

</body>
</html>
