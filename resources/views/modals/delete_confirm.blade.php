<!-- Modal -->
<div class="modal fade delete-confirm-modal" tabindex="-1" role="dialog" aria-labelledby="delete_confirm_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="delete_confirm_modal">Are You sure?</h4>
      </div>
      <div class="modal-body">
        <p>
			Are you sure you want to delete this item?
		</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        <button type="button" class="btn btn-primary yes-button">Yes</button>
      </div>
    </div>
  </div>
</div>
