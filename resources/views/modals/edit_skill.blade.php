<!-- Modal -->
<div class="modal fade edit-skill-modal" tabindex="-1" role="dialog" aria-labelledby="edit_skill_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4>Edit Skill</h4>
		</div>
		<div class="modal-body">
		  @include('forms.edit_skill')
		</div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
