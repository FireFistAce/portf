<!-- Modal -->
<div class="modal fade show-image-modal" tabindex="-1" role="dialog" aria-labelledby="show_image_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <img class="full-screen-image img-fluid" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary delete-button">Delete</button>
      </div>
    </div>
  </div>
</div>
