@extends('layouts.project')
@section('content')
	<div class="header container-fluid" style="">


		<p class='project-name'>
			{{$project->name}}
		</p>
		<p class='project-desc'>
			{{$project->description}}
		</p>
	</div>
	<div class="slider-container container-fluid" style="background-color:#1B2539;text-align:center;padding:4% 4% 4% 4%;">
		@include('sliders.main_slider')
	</div>
	<div class="project-details container-fluid ">
		<div class="project-details-header container">
			<div class="project-details-name pull-left">
				<p style="font-size:1.125em;font-family:sans";><span style="font-weight:bold;font-size:1.125em;font-family:sans;">Project:</span>
					&nbsp {{$project->name}}
					<br />
					<span style="font-weight:bold;font-size:1.125em;font-family:sans;">Since:</span>&nbsp&nbsp&nbsp&nbsp {{$project->date}}
				</p>
			</div>
			<div class="project-details-visit pull-right" >
				<a href="{{$project->URL}}" class="btn round-button-dark" style="border-radius:15px;">Visit</a>
			</div>
			<div style="clear:both;">

			</div>
		</div>
		<br />
		<br />
		<div class="project-details-body container">
			<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 text-justify" >
				<h4 style="color:#333;letter-spacing:0.25em">Project Details</h4>
				<hr style="border-color:#FC9D0D"/>
				<p style="line-height:1.75em;font-size:1.125em;">
					{{$project->description}}
				</p>
			</div>
			<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 text-justify">
				<h4 style="color:#333;letter-spacing:0.25em">Skills</h4>
				<hr style="border-color:#FC9D0D"/>
				@foreach($project->skills as $skill)
					<button class="btn btn-sm btn-default" >
						{{$skill->name}}
					</button>

				@endforeach
			</div>
			<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 text-justify">
				<h4 style="color:#333;letter-spacing:0.25em">Categories</h4>
				<hr style="border-color:#FC9D0D"	/>
				<button class="btn btn-sm btn-default" >
					{{$project->category->name}}
				</button>

			</div>
		</div><!--project-details-body-->

	</div>
	<div class="nav-project container-fluid" style=>
		@if($prev)
			<a href="{{url('project/'.$prev->id)}}" class ="prev-project pull-left"><span class="glyphicon glyphicon-chevron-left"></span><br/>Previous Project</a>
		@endif
		@if($next)
			<a href="{{url('project/'.$next->id)}}" class ="next-project pull-right"><span class="glyphicon glyphicon-chevron-right pull-right"></span><br/>Next Project</a>
		@endif
	</div>
@endsection
