<table class="projects-table table col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Client</th>
			<th>
				URL
			</th>
			<th>
				Skills
			</th>
			<th>
				Category
			</th>
			<th>
				Images(Click image to Edit it)
			</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>

            @foreach($projects as $project)
            <tr>
                <td id="id">{{$project->id}}</td>
                <td id="name">{{$project->name}}</td>
                <td id="client">{{$project->client}}</td>
				<td>
					<a href="{{$project->URL}}"	>{{$project->URL}}</a>
				</td>
				<td>
					@foreach($projectSkills as $ps)
						@if($project->id == $ps->project_id)
							<button class="btn btn-xs btn-default" >
								{{$skills->find($ps->skills_id)->name." "}}
							</button>

						@endif
					@endforeach
				</td>
				<td>
					<button class="btn btn-default btn-xs">
						{{$project->category->name." "}}
					</button>
				</td>
				<td id="project_images">
					<div class="table-image-container container-fluid">
					@foreach($images as $image)
						@if($image->project_id == $project->id)
							<div class="project-img-thumbnail-container">
								<img class="project-img-thumbnail rounded-circle" src="{{$image->name}}" data-image-id="{{$image->id}}"/>
							</div>
						@endif
					@endforeach
				</td>
                <td><a href="/project/{{$project->id}}/edit" class="btn btn-default edit-project-button ">Edit</td>
                <td><button class = 'btn btn-danger delete-project-button' data-project-id={{$project->id}}>Delete</td>
            </tr>
            @endforeach
        </tbody>
</table>
