<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','HomeController@index');
Auth::routes();
Route::get('register','HomeController@index');

Route::get('admin','AdminController@admin');

Route::get('project/{id}/get/images','ProjectController@getImages');
Route::get('project/{id}/delete','AdminController@deleteProject');
Route::get('project/{id}/edit','AdminController@EditProject');
Route::post('project/{id}/update','AdminController@updateProject');
Route::get('project/{id}','ProjectController@getProject');
Route::post('project/add','AdminController@setProject');

Route::get('image/{id}/delete','AdminController@deleteImage');

Route::post('category/add','AdminController@setCategory');
Route::get('category/{id}/delete','AdminController@deleteCategory');
Route::post('category/{id}/update','AdminController@updateCategory');

Route::post('skill/add','AdminController@setSkill');
Route::get('skill/{id}/delete','AdminController@deleteSkill');
Route::get('skill/{id}/update','AdminController@updateSkill');

Route::get('home', 'HomeController@index');
